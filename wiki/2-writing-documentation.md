# The syntax

## How to write documentation

In general the syntax is used to describe a function/method:
 - what it does
 - what arguments it should receive
 - what output it will give

This is how it's written:
```javascript
/**
  DESCRIPTION: method's description here
  ARGUMENTS: (
    firstArgumentName <firstAgumentType> « first argument's comment »,
    secondArgumentName <secondAgumentType> « second argument's comment »,
  )
  RETURN: <whatIsReturnedByThisMethod> « description of the return »
  OTHER SECTION: you can add any amout of sections that are useful to you to describe what the code is doing
  YET ANOTHER SECTION:
    other useful
    things
    to describe
*/
```
Almost anything is optional, you write what is necessary, or feels necessary to you.

## Some examples

A very basic example:
```javascript
/**
  DESCRIPTION: list all files in a directory
  ARGUMENTS: (
    !directoryPath <string> « path to the directory »,
    ?recursive <boolean>@default=false « if true, will list files recursively »,
  )
  RETURN: <string[]> « array of paths »
*/
function ls (directoryPath, recursive) {
  // ... your code here
};
```

A more complex example:
```javascript
/**
  DESCRIPTION: create an input with a nice display
  ARGUMENTS: ({
    !$container: <jqueryProcessable> « input container »,
    !name: <string> « input name »,
    !type: <inputType> « the type of input to create »,
    ?changed: <function(value<valueResult>){@this=<myCreatedInput>}> « if defined this function will be ran when the input is changed »,
    ?orientation: <"vertical"|"horizontal">@default="vertical",
    ?objectResult: <boolean>@default=true « if true, get, set, and changed methods will return value in the shape: { name: <string>, value: <any>, } »,
    ¡internalMethod: <function(<string>):<void>> « a method that shouldn't be defined but will be set internally only, if necessary »,

  }){ @this=<contextObject> }
  RETURN: <myCreatedInput> « object with infos and methods that can be executed on the input »
  TYPES:
    jqueryProcessable = <any string or object that jquery can process and get elements from>
    inputType = <"normal"|"number"|"date"|"slider"|"object"|"array"|"color">
    myCreatedInput = <{
      get: <function(ø){@this=<myCreatedInput>}:<valueResult>> « get's the value of the input »
      set: <function(value<any>){@this=<myCreatedInput>}:<valueResult>> « set's the value of the input »
    }>
    valueResult = <
      |any                              // if objectResult = false
      |{ name: <string>, value: <any>}  // if objectResult = true
    >
    contextObject = <{
      first: "contextString",
      second: "contextString",
      this: "object will be `this` value when inputCreator is called",
    }>
*/
function inputCreator (options) {
  // ... your code here
};
```

## All the symbols

| Symbol | Description | Example usage |
|:------:|:-----------:|:-------------:|
|< >| describes a type | labelDisplay \<boolean\> |
|«  »| describes a comment | labelDisplay \<boolean\> « if false will not display this element's label » |
|!| is mandatory | !labelDisplay \<boolean\> |
|?| is optional | ?labelDisplay \<boolean\> |
|¡| shouldn't be defined | ¡argUsedOnlyInternally \<any\> |
|\|| in types this describe that one or the other is valid (like javscript's \|\|) | labelLayout \<"stacked"\|"hidden"\|"inline"\> |
|\| or \|--| in options names, there are different ways to specify that some options are equivalent to each other, see below for details | see below |
|--| in options names, !-- or \|-- or ?-- is used to group different options to specify that they go with one another, see below for details | see below |
|@default=| default value | labelDisplay \<boolean\>@default=true |
|ø| to specify that this function doesn't receive any argument | \<function(ø)\> |
|:| to specify the return of a function | \<function(ø):\<someType\>\> |
|void| the type to say that a function returns nothing | \<function(ø):\<void\>\> |
|any| the type to say that you may pass any type of value ;) | someArgument \<any\> |
|{@this=<>}| the value/type of `this` in the function | \<function(ø){this=\<someType\>}> |
|·| to reference the argument of a method <br>(careful not to confuse "·" with the classic "." that references subvalues of objects) | func: \<function(opts)\><br>func·opts |

## The or statement in option name `|` or `|--`

The `|` in option names is used to express that all those options are equivalent, and that only one of them should be passed.

There are many ways to use `|` in options names:

#### In the same line

This is like aliases (different names for the same thing).

```javascript
someOption|otherOption <type> « comment »
```

*Examples:*

```javascript
image|picture|photo <string> « image url »
!name|title <string> // here you "must" set either name or title
```

#### Multiline

If the different options have different types, or you need more space to write comments... you can use the following syntax:

```
|someOption <type> « comment »,
|someOtherOption <type> « comment »,
|yetAnotherOption <type> « comment »,
```

*Example:*

```
|queryString <string>
|queryObject <object>
```

#### Options groups

For yet more flexibility, for example to express that one should either pass option "a", or option "b, c and d", you can use the following syntax:

```
|-- !youCanSetThisOption <type> « comment »,
|-- !orYouCan <type> « comment »,
    !setThose <type> « comment »,
    !threeOtherOptionsInstead <type> « comment »,
|-- !butIfYouPreferYouCanJustSetThisOne <type> « comment »,
    ?andMaybeThisOneThatGoesWithItJustIfYouLike <type> « comment »,
```

*See the next section about grouping options for an example.*

## Grouping options with `?--`, `!--`, `|--`

The `--` syntax allows you to group options, and specify that they have a common status, or that they must be used conjointly. See the example below for different usage cases.

*Example:*

```
<{
  |-- !url: <string> « full url, just without query and hash »,
  |-- !protocol,
      ?-- !username,
          !password,
      |-- domain,
      |-- !hostname,
          !port,
      !path,
  ?-- |queryString,
      |queryObject,
  ?hash,
}>
```

This example describes that, one should pass either:
 - url

or

 - protocol *and*
 - username and password or none of them *and*
 - domain or hostname and port *and*
 - path

and whichever were passed before, pass:

- queryString or queryObject or none of them
- hash (just if you'd like to)
