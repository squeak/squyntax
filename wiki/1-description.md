# Why squyntax?

The goal of squyntax is to have a concise and straightforwardly understandable description of any javascript variable, to describe what it should or should not be for your code to work.

This is similar in some ways to what typescript aim to achieve, but without changing the language, without constraining anything. The syntax is just used in comments to describe the code.
